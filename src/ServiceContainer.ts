import { ServiceConstructorMapType, Service, ServiceMapType } from "./Service";
import { Logger } from "winston";

// Ordered
enum ServiceEachMode {
	NORMAL = 0,
	REVERSED = 1
}

// Service list
type ServiceListItem = {
	service: Service;
	dependencies: string[];
	key: string;
};

/**
 * Container de serviços
 *
 * Controla todos os serviços
 */
export class ServiceContainerGeneric<
	AppType,
	T extends ServiceConstructorMapType = ServiceConstructorMapType,
	ParentType = AppType
> implements Service {
	/// App instance
	readonly app: AppType;
	/// Parent instance
	readonly parent: ParentType;
	/// Logger
	readonly logger: Logger;

	/// Service instance map
	protected serviceMap?: ServiceMapType<T> = null;
	/// Service list ordered
	protected serviceListOrdered?: ServiceListItem[] = null;
	/// Service constructor map
	protected serviceConstructorMap: T;

	/// Get the services
	get services(): ServiceMapType<T> | null {
		return this.serviceMap;
	}

	/// For each service
	async eachService<U>(
		cb: string | ((service: Service) => any),
		args: any[] = [],
		mode: ServiceEachMode = ServiceEachMode.NORMAL
	): Promise<any> {
		let serviceList = this.serviceListOrdered;
		if (mode === ServiceEachMode.REVERSED) serviceList = serviceList.reverse();

		if (typeof cb === "string") {
			const method: string = cb;
			cb = (service: any) => {
				service.logger.info(`Calling ${method}`, { method });
				if (service[method]) return service[method].apply(service, args);
			};
		}
		const results: any = {};
		for (const serviceItem of serviceList) {
			results[serviceItem.key] = await cb(serviceItem.service);
		}
		return results;
	}

	/// For each service in parallel
	async eachServiceParallel<U>(
		cb: string | ((service: Service) => any),
		args: any[] = [],
		mode: ServiceEachMode = ServiceEachMode.NORMAL
	): Promise<any> {
		let serviceList = this.serviceListOrdered;
		if (mode === ServiceEachMode.REVERSED) serviceList = serviceList.reverse();

		if (typeof cb === "string") {
			const method: string = cb;
			cb = (service: any) => {
				service.logger.info(`Calling ${method}`, { method });
				if (service[method]) return service[method].apply(service, args);
			};
		}

		const promises = [];
		const results: any = {};
		for (const serviceItem of serviceList) {
			promises.push(
				Promise.resolve()
					.then(() => {
						return (<any>cb)(serviceItem.service);
					})
					.then((result: any) => {
						results[serviceItem.key] = result;
					})
			);
		}
		return Promise.all(promises).then(() => results);
	}

	/// Perform the setup of the services
	async setup(): Promise<void> {
		this._createServices();
		await this.eachService("setup");
	}
	/// Run the services
	async run(): Promise<void> {
		await this.eachService("run");
	}
	/// Close every child service
	async close(): Promise<void> {
		await this.eachService("close", [], ServiceEachMode.REVERSED);
	}

	/// Create the services
	private _createServices() {
		const services: any = {};
		const serviceList: ServiceListItem[] = [];
		for (const serviceKey in this.serviceConstructorMap) {
			const serviceConstructor = this.serviceConstructorMap[serviceKey];
			const serviceInstance: any = new serviceConstructor(serviceKey, this.app || this, this);
			serviceInstance.app = this.app || this;
			serviceInstance.parent = this;
			serviceInstance.logger = this.logger ? this.logger.child({ name: serviceKey }) : null;
			services[serviceKey] = serviceInstance;
			serviceList.push({
				service: serviceInstance,
				dependencies: serviceInstance.dependencies || [],
				key: serviceKey
			});
		}
		this.serviceMap = services;
		this.serviceListOrdered = this._orderServices(serviceList);
	}
	/// Order the services
	private _orderServices(serviceList: ServiceListItem[]): ServiceListItem[] {
		serviceList = [...serviceList];
		serviceList.sort((a: ServiceListItem, b: ServiceListItem) => {
			if (a.dependencies.indexOf(b.key) >= 0) {
				return 1;
			} else if (b.dependencies.indexOf(a.key) >= 0) {
				return -1;
			}
			const priorityA: number = a.service.priority | 0;
			const priorityB: number = b.service.priority | 0;
			return priorityB - priorityA;
		});
		return serviceList;
	}
}
