import { ServiceConstructorMapType } from './Service';
import { ServiceContainerGeneric } from './ServiceContainer';
import { Logger, createLogger, transports } from 'winston';

/**
 * Main application class
 *
 * Controls every single service
 */
export class AppBase<T extends ServiceConstructorMapType, OptionsType = any> extends ServiceContainerGeneric<null, T> {
	readonly options: OptionsType;
	readonly logger: Logger;

	// Constructor for the app
	constructor() {
		super();
	}

	/// Create the default logger
	protected createLogger(): Promise<Logger> | Logger {
		return createLogger({
			transports: [new transports.Console()]
		});
	}

	/// Processa os argumentos
	processArgv(argv: string[]): OptionsType {
		return null;
	}

	/// Run the app from the argv
	async runFromArgv(argv: string[]): Promise<this> {
		return this.doRun(() => this.processArgv(argv));
	}
	/// Run the app using the given options
	async runFromOptions(options: OptionsType): Promise<this> {
		return this.doRun(() => options);
	}
	/// Do run the application
	private async doRun(options: () => Promise<OptionsType> | OptionsType): Promise<this> {
		process.on('SIGINT', createTimeoutHandlerHelper(() => this.close()));
		const app: any = this;
		app.app = app;
		app.parent = null;
		app.options = (await options()) || {};
		app.logger = await this.createLogger();
		await this.setup();
		await this.run();
		return this;
	}
}

// Timeout
function createTimeoutHandlerHelper(handler, deltaTolerance = 10000) {
	let lastCalled = 0;
	return async function() {
		const now = Date.now();
		const delta = now - lastCalled;
		if (delta <= deltaTolerance) {
			process.stderr.write('Exiting application\n');
			process.exit();
			return;
		}

		lastCalled = now;
		let timeout = setTimeout(function() {
			process.stderr.write('Press ^C again to kill the application\n');
			timeout = null;
		}, 250);
		timeout.unref();
		await handler.call();
	};
}
