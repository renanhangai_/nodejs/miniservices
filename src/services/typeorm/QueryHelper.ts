import { EntityManager } from 'typeorm';
import GraphqlFields from 'graphql-fields';
import getValue from 'get-value';
import { validateObject, ValidatorContext } from '../../utils/validate';

/// Tipos
export type QueryMapDefinitionRelationType =
	| string
	| {
			key: string;
			field: string;
			targetEntityClass?: any;
	  };
export type QueryMapDefinitionItemType = {
	key?: string;
	entityClass: any;
	relations?: Array<QueryMapDefinitionRelationType>;
};
export type QueryMapDefinitionType = {
	[key: string]: QueryMapDefinitionItemType;
};

/// Get an entity manager according to graphql context
export type QueryHelperEntityManagerGetter = (context: any) => EntityManager;
export type QueryHelperWhereResolverOptions<ValueType> = {
	args: any;
	value: ValueType;
};

export type QueryHelperResolverOptions<ValueType, T> = {
	entityClass: { new (...args: any[]): T };
	field?: string | null;
	where?: (options: QueryHelperWhereResolverOptions<ValueType>) => any;
	validate?: (context: ValidatorContext) => any;
	raw?: any;
};

/**
 * Helper class to build typeorm
 */
export class QueryHelperDefinition {
	private readonly queryMap = null;
	private readonly entityManagerGetter: QueryHelperEntityManagerGetter;

	/**
	 * @param definition
	 */
	constructor(queryMapDefinition: QueryMapDefinitionType, entityManagerGetter?: QueryHelperEntityManagerGetter) {
		this.entityManagerGetter = entityManagerGetter || defaultEntityManagerGetter;
		this.queryMap = new Map();
		for (const key in queryMapDefinition) {
			const definition = { key, ...queryMapDefinition[key] };
			this.queryMap.set(definition.entityClass, definition);
		}
	}

	/// Pega a definição
	public getDefinition(entityClass: any): QueryMapDefinitionItemType {
		return this.queryMap.get(entityClass) || {};
	}

	/// Acha o objeto
	find(entityManager: EntityManager, entityClass, info, where, options?) {
		if (!entityManager) throw new Error('Invalid entity manager');
		options = options || {};

		const fields = GraphqlFields(info);

		const relations: string[] = [];
		this.mergeRelations(fields, relations, entityClass, [], []);

		const method = options.findOne ? 'findOne' : 'find';
		return entityManager[method].call(entityManager, entityClass, {
			where: removeUndefined(where),
			relations,
			...options.raw
		});
	}

	/// Faz um merge das relações para criar a query
	private mergeRelations(
		fields: any,
		relations: string[],
		entityClass: any,
		fieldPath: string[],
		relationPath: string[]
	) {
		const definition = this.getDefinition(entityClass);
		if (!definition) return;

		for (const key in definition.relations) {
			const relation = definition.relations[key];
			// Verifica a relação se string
			if (typeof relation === 'string') {
				const currentFieldPath = fieldPath.concat(relation);
				if (getValue(fields, currentFieldPath)) {
					const currentRelationPath = relationPath.concat(relation);
					relations.push(currentRelationPath.join('.'));
				}
				continue;
			}

			const currentFieldPath = fieldPath.concat(relation.key);
			if (getValue(fields, currentFieldPath)) {
				const currentRelationPath = relationPath.concat(relation.field);
				relations.push(currentRelationPath.join('.'));
				if (relation.targetEntityClass) {
					this.mergeRelations(fields, relations, relation.targetEntityClass, currentFieldPath, currentRelationPath);
				}
			}
		}
	}

	/// Create the resolver for a single item
	createResolverOne<ValueType = any, T = any>(options: QueryHelperResolverOptions<ValueType, T>) {
		return (value, args, context, info) => {
			if (options.field && value[options.field]) return value[options.field];
			let where = options.where ? options.where({ args, value }) : null;
			if (options.validate) {
				where = validateObject(where, options.validate);
			}
			const entityManager = this.entityManagerGetter(context);
			return this.find(entityManager, options.entityClass, info, where, {
				raw: options.raw,
				findOne: true
			});
		};
	}

	/// Create a resolver for many itens
	createResolver<ValueType = any, T = any>(options: QueryHelperResolverOptions<ValueType, T>) {
		return (value, args, context, info) => {
			if (options.field && value[options.field]) return value[options.field];
			let where = options.where ? options.where({ args, value }) : null;
			if (options.validate) {
				where = validateObject(where, options.validate);
			}
			const entityManager = this.entityManagerGetter(context);
			return this.find(entityManager, options.entityClass, info, where, {
				raw: options.raw
			});
		};
	}
}

/// Create the definition
export function createQueryHelper(
	queryMapDefinition: QueryMapDefinitionType,
	entityManagerGetter?: QueryHelperEntityManagerGetter
): QueryHelperDefinition {
	return new QueryHelperDefinition(queryMapDefinition, entityManagerGetter);
}

/// Default getter for EntityManager
function defaultEntityManagerGetter(context: any) {
	return context.entityManager || (context.connection && context.connection.manager);
}

function removeUndefined(obj: any) {
	const output: any = {};
	for (const key in obj) {
		const v = obj[key];
		if (v === void 0) continue;
		output[key] = v;
	}
	return output;
}
