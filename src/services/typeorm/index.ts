import { Service } from "../../Service";
import { ConstructorType, TypeOrFunctor } from "../../utils/Types";
import { Logger } from "winston";
import { createConnection, Connection, ConnectionOptions, EntityManager } from "typeorm";
import retry from "async-retry";
import { createQueryHelper } from "./QueryHelper";

type ServiceConnectionOptions = TypeOrFunctor<ConnectionOptions>;

type ServiceConnectionOptionsMap = {
	default: ServiceConnectionOptions;
	[key: string]: ServiceConnectionOptions;
};
type ServiceConnectionMap<Options extends ServiceConnectionOptionsMap> = { [key in keyof Options]: Connection };

export interface IDabaseService<ServiceConnectionOptionsMapType extends ServiceConnectionOptionsMap> extends Service {
	readonly connections: ServiceConnectionMap<ServiceConnectionOptionsMapType>;
	readonly connection: Connection;
}

/**
 * Create the typeorm database service
 */
export const ServiceDefinition = {
	/// Create the queryHelper
	createQueryHelper,
	/**
	 * Create the new connection map
	 *
	 * @param ServiceBase The service base
	 * @param connectionOptionsMap The connections
	 */
	createService<
		ServiceBaseType extends ConstructorType<any>,
		ServiceConnectionOptionsMapType extends ServiceConnectionOptionsMap
	>(
		ServiceBase: ServiceBaseType,
		connectionOptionsMap: TypeOrFunctor<ServiceConnectionOptionsMapType>
	): ConstructorType<IDabaseService<ServiceConnectionOptionsMapType>> {
		///@ts-ignore
		return class extends ServiceBase implements IDabaseService<ServiceConnectionOptionsMapType> {
			private _connectionMap: ServiceConnectionMap<ServiceConnectionOptionsMapType>;

			async setup() {
				this._connectionMap = await createConnectionMap(connectionOptionsMap, this.logger);
			}

			async close() {
				const connectionMap = this._connectionMap;
				this._connectionMap = null;
				for (const key in connectionMap) {
					await connectionMap[key].close();
				}
			}

			get connections(): ServiceConnectionMap<ServiceConnectionOptionsMapType> {
				return this._connectionMap;
			}

			get connection(): Connection {
				return this._connectionMap.default;
			}
		};
	}
};
export type ServiceDefinitionType = typeof ServiceDefinition;

/// Create the conection map
async function createConnectionMap<ServiceConnectionOptionsMapType extends ServiceConnectionOptionsMap>(
	connectionOptionsMap: TypeOrFunctor<ServiceConnectionOptionsMapType>,
	logger?: Logger
): Promise<ServiceConnectionMap<ServiceConnectionOptionsMapType>> {
	if (typeof connectionOptionsMap === "function") connectionOptionsMap = connectionOptionsMap();

	const connectionMap: any = {};
	for (const key in connectionOptionsMap) {
		let connectionOptions: ServiceConnectionOptions = connectionOptionsMap[key];
		if (typeof connectionOptions === "function") connectionOptions = connectionOptions();
		if (!connectionOptions) continue;
		connectionMap[key] = await retry(async () => {
			try {
				if (logger) logger.info("Trying to connect");
				return await createConnection(<ConnectionOptions>connectionOptions);
			} catch (e) {
				if (logger) logger.error(e);
				throw e;
			}
		});
	}
	return connectionMap;
}
