import { ServiceDefinitionType as HapiServiceDefinitionType } from "./hapi";
import { ServiceDefinitionType as TypeormServiceDefinitionType } from "./typeorm";

export const Services = {
	hapi(): HapiServiceDefinitionType {
		return require("./hapi").ServiceDefinition;
	},
	typeorm(): TypeormServiceDefinitionType {
		return require("./typeorm").ServiceDefinition;
	}
};
