import { createService } from './Service';
import { createRouteManager } from './RouteManager';

export const ServiceDefinition = {
	createService,
	createRouteManager
};
export type ServiceDefinitionType = typeof ServiceDefinition;

export { RouteRequest } from './RouteManager';
export { RouteValidator } from './route/Context';
