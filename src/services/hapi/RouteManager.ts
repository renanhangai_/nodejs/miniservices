import path from 'path';
import getValue from 'get-value';
import { MiniserviceHapiService } from './Service';
import {
	Server,
	RouteOptions as HapiRouteOptions,
	Request as HapiRequest,
	ResponseToolkit as HapiResponseToolkit
} from 'hapi';
import { RouteContextUtils } from './route/Context';

/**
 * Options for every route
 */
export type RouteOptions<ExtraOptions extends any> = {
	/// Method for the route
	method: 'GET' | 'POST';
	/// Path for the route
	path: string;
} & Partial<ExtraOptions>;
export type RouteOptionsParam<ExtraOptions extends any> = Partial<Pick<RouteOptions<ExtraOptions>, 'method' | 'path'>> &
	Partial<ExtraOptions>;
/**
 * Options for the router class
 */
export type RouterOptions = {
	/// Base path
	basePath?: string;
};
/**
 * Options to create the plugin
 */
export type RoutePluginOptions<ExtraOptions, Context> = {
	/// The route map
	routes: { [key: string]: { new (...args: any): any } };
	/// The service class
	service: MiniserviceHapiService;
};

/**
 * The request object
 */
export type RouteRequest<ExtraOptions, Context> = {
	/// The context
	request: HapiRequest;
	/// The response toolkit
	h: HapiResponseToolkit;
	/// The route
	route: Readonly<RouteOptions<ExtraOptions>>;
} & Context;

/// Middleware
export type RouteMiddleware<ExtraOptions, Context> = (
	request: RouteRequest<ExtraOptions, Context>,
	next: () => any
) => any;

/**
 * Options passed when creating the context
 */
export type RouteManagerOptionsContextCallback<ExtraOptions, DataType> = {
	/// The request
	request: HapiRequest;
	/// THe response
	h: HapiResponseToolkit;
	/// The route
	route: RouteOptions<ExtraOptions>;
	/// The service
	service: MiniserviceHapiService;
	/// Utility class to generate the context
	utils: RouteContextUtils;
	/// The data returned by the data function
	data: DataType;
};
/**
 * Options passed when creating the manager
 */
export type RouteManagerOptions<ExtraOptions, Context, DataType = any> = {
	/// Data to setup the context
	data?: (() => DataType) | (() => Promise<DataType>);
	/// Create the context
	context?:
		| ((options: RouteManagerOptionsContextCallback<ExtraOptions, DataType>) => Context)
		| ((options: RouteManagerOptionsContextCallback<ExtraOptions, DataType>) => Promise<Context>);
	/// Normalize the route options
	normalizeRouteOptions?: (options: RouteOptions<ExtraOptions>) => HapiRouteOptions;
	/// Middlewares
	middlewares?: RouteMiddleware<ExtraOptions, Context>[];
};
/**
 * Context passed when creating the plugin
 */
export type RoutePluginContext<ExtraOptions, Context> = {
	data?: any;
	pluginOptions: RoutePluginOptions<ExtraOptions, Context>;
	options: RouteManagerOptions<ExtraOptions, Context>;
};

/**
 * Router class
 */
export class Router<ExtraOptions, Context> {
	/// Construtor
	constructor(
		private readonly routeManager: RouteManager<ExtraOptions, Context>,
		public readonly options: Readonly<RouterOptions>
	) {}
	/// Apply the route
	applyRoutes(server: Server, routePluginContext: RoutePluginContext<ExtraOptions, Context>, targetClass: any) {
		const routes = getValue(targetClass, ['$router', 'routes']);
		if (!routes) return;

		const instance = new targetClass();
		for (const { key, route } of routes) {
			this.applyRoute(server, routePluginContext, instance, key, route);
		}
	}
	/**
	 * Apply a single route
	 */
	applyRoute(
		server: Server,
		routePluginContext: RoutePluginContext<ExtraOptions, Context>,
		instance: any,
		key: string,
		route: RouteOptions<ExtraOptions>
	) {
		route = { ...route };
		let handler: any;
		handler = async (hapiRequest, h) => {
			const request = await this.createRequest(routePluginContext, route, hapiRequest, h);
			return this.invokeHandler(routePluginContext, instance, key, request);
		};
		server.route({
			method: route.method,
			path: path.join('/', this.options.basePath || '', route.path),
			handler,
			options: {
				...routePluginContext.options.normalizeRouteOptions(route)
			}
		});
	}
	/**
	 * Create the request
	 */
	async createRequest(
		routePluginContext: RoutePluginContext<ExtraOptions, Context>,
		route: RouteOptions<ExtraOptions>,
		hapiRequest: HapiRequest,
		h: HapiResponseToolkit
	): Promise<RouteRequest<ExtraOptions, Context>> {
		const context = await routePluginContext.options.context({
			request: hapiRequest,
			h,
			route,
			service: routePluginContext.pluginOptions.service,
			data: routePluginContext.data,
			utils: new RouteContextUtils(hapiRequest)
		});
		return {
			request: hapiRequest,
			h,
			route,
			...context
		};
	}
	/**
	 * Invoke the handler
	 * @param request
	 * @param h
	 */
	invokeHandler(
		routePluginContext: RoutePluginContext<ExtraOptions, Context>,
		instance: any,
		key: string,
		request: RouteRequest<ExtraOptions, Context>
	) {
		const middlewares = routePluginContext.options.middlewares || [];
		let i = 0;
		const next = async (err?: any) => {
			if (err) {
				throw err;
			}
			if (i >= middlewares.length) {
				return instance[key].call(instance, request);
			}
			const middleware = middlewares[i++];
			return middleware.call(null, request, next);
		};
		return next();
	}
}

/**
 * Route Manager
 */
export class RouteManager<ExtraOptions = any, Context = any, DataType = any> {
	/// Routers
	private readonly routersMap: Map<any, Router<ExtraOptions, Context>> = new Map();

	/// Construtor
	constructor(private readonly options: RouteManagerOptions<ExtraOptions, Context>) {}

	/**
	 * Create a new router
	 * @param routerOptions The route
	 */
	addRouter(target: any, routerOptions?: RouterOptions) {
		this.routersMap.set(target, new Router(this, routerOptions || {}));
	}
	/**
	 * Create a new route
	 * @param route The route
	 */
	applyRoutes(server: Server, routePluginContext: RoutePluginContext<ExtraOptions, Context>, routes: any) {
		for (const key in routes) {
			const routeClass = routes[key];
			const router = this.routersMap.get(routeClass) || new Router(this, {});
			router.applyRoutes(server, routePluginContext, routeClass);
		}
	}
	/// Create decorators
	getDecorators() {
		const ThisManager = this;
		const Decorators = {
			/// Mark the current class a new router
			Router(routerOptions?: RouterOptions | string) {
				if (typeof routerOptions === 'string') {
					routerOptions = { basePath: routerOptions };
				}
				return function(target) {
					ThisManager.addRouter(target, routerOptions as RouterOptions);
				};
			},
			/**
			 * Create a new route
			 * @param route The route
			 */
			Route(route: RouteOptions<ExtraOptions>) {
				return function(target, key, description) {
					target.constructor.$router = target.constructor.$router || {};
					target.constructor.$router.routes = target.constructor.$router.routes || [];
					target.constructor.$router.routes.push({
						key,
						route
					});
				};
			},
			/// Create a get route
			GET(route: RouteOptionsParam<ExtraOptions> | string) {
				if (typeof route === 'string') {
					route = {
						method: 'GET',
						path: route
					} as any;
				} else {
					route = {
						method: 'GET',
						...route
					};
				}
				return Decorators.Route(route as any);
			},
			/// Create a post route
			POST(route: RouteOptionsParam<ExtraOptions> | string) {
				if (typeof route === 'string') {
					route = {
						method: 'POST',
						path: route
					} as any;
				} else {
					route = {
						method: 'POST',
						...route
					};
				}
				return Decorators.Route(route as any);
			}
		};
		return Decorators;
	}
	/// Get the plugin
	getPlugin<T extends Context = Context>(name: string = 'routes') {
		return {
			name,
			register: async (server: Server, options: RoutePluginOptions<ExtraOptions, T>) => {
				let data = null;
				if (this.options.data) {
					data = await this.options.data();
				}

				const routePluginContext = {
					data,
					pluginOptions: options,
					options: this.options
				};
				await this.applyRoutes(server, routePluginContext, options.routes);
			}
		};
	}
}

/// Create the route manager
export function createRouteManager<ExtraOptions, Context, DataType = any>(
	options?: RouteManagerOptions<ExtraOptions, Context, DataType>
) {
	const manager = new RouteManager<ExtraOptions, Context, DataType>(options || {});
	const Plugin = manager.getPlugin();
	const Decorators = manager.getDecorators();
	return {
		GET: Decorators.GET,
		POST: Decorators.POST,
		Router: Decorators.Router,
		Plugin
	};
}
