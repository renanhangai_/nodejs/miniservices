import { Service } from "../../Service";
import { ConstructorType, TypeOrFunctor, ConstructorInstanceType } from "../../utils/Types";
import { Server, ServerOptions } from "hapi";
import { ApolloServer, Config as ApolloServerConfig } from "apollo-server-hapi";

/// The service
export interface MiniserviceHapiService extends Service {
	readonly server: Server;
}

/// Options for the graphql
export type MiniservicesHapiOptionsGraphql<ServiceBaseConstructorType extends ConstructorType<any>> = {
	path: string;
	apollo: TypeOrFunctor<ApolloServerConfig, ServiceBaseConstructorType>;
};

/// Callback
export type MiniservicesHapiOptionsCallback = (server: Server, app: any) => any;

/// Hapi service
export type MiniserviceHapiOptions<ServiceBaseConstructorType extends ConstructorType<any>> = {
	server: ServerOptions;
	setup?: (service: ConstructorInstanceType<ServiceBaseConstructorType>) => any;
	auth?: MiniservicesHapiOptionsCallback;
	register?: MiniservicesHapiOptionsCallback;
	graphql?: MiniservicesHapiOptionsGraphql<ServiceBaseConstructorType>;
};

/**
 * Create the HAPI service
 *
 * @param ServiceBase The service to be used as a base type
 * @param options The options to pass to the hapi server
 */
export function createService<ServiceBaseConstructorType extends ConstructorType<any>>(
	ServiceBase: ServiceBaseConstructorType,
	options?: TypeOrFunctor<MiniserviceHapiOptions<ServiceBaseConstructorType>>
): ConstructorType<MiniserviceHapiService> {
	///@ts-ignore
	return class extends ServiceBase implements MiniserviceHapiService {
		private _server: Server;
		private _apollo: ApolloServer;
		private _options: MiniserviceHapiOptions<ServiceBaseConstructorType>;

		async setup() {
			this._options = typeof options === "function" ? options(this) : options;
			if (this._options.setup) {
				const status = await this._options.setup.call(this, this);
				if (status === false) return false;
			}
			this._server = new Server(this._options.server);
			if (this._options.auth) await this._options.auth.call(this, this._server, this.app);
			await this._setupGraphql();
			if (this._options.register) await this._options.register.call(this, this._server, this.app);
		}

		async run() {
			if (this._server) await this._server.start();
		}

		async close() {
			if (this._apollo) {
				this._apollo.stop();
				this._apollo = null;
			}
			if (this._server) {
				this._server.stop();
				this._server = null;
			}
		}

		// API server
		get server(): Server {
			return this._server;
		}

		async _setupGraphql() {
			if (!this._options.graphql) return;

			const graphql = this._options.graphql;
			const config = typeof graphql.apollo === "function" ? await graphql.apollo.call(this, this.app) : graphql.apollo;

			this._apollo = new ApolloServer(config);
			await this._apollo.applyMiddleware({
				path: graphql.path,
				app: this._server
			});
		}
	};
}
