import { Request as HapiRequest } from 'hapi';
import { ValidatorSchemaParam, ValidatedObject, Validate } from '../../../utils/validate';

/**
 * Context validator
 */
export type RouteValidator = {
	/**
	 * Validates agains an object
	 */
	<T>(schema: ValidatorSchemaParam<T>, obj: any): ValidatedObject<T>;
	/**
	 * Validate agains the parameters
	 * @param schema
	 */
	params<T>(schema: ValidatorSchemaParam<T>): ValidatedObject<T>;
	query<T>(schema: ValidatorSchemaParam<T>): ValidatedObject<T>;
	payload<T>(schema: ValidatorSchemaParam<T>): ValidatedObject<T>;
};

/**
 * Utility classes to help generate context
 */
export class RouteContextUtils {
	/// Create the utility class
	constructor(private readonly request: HapiRequest) {}
	/**
	 * Create the validator
	 */
	createValidator(): RouteValidator {
		// Cria o validador do request
		const v: RouteValidator = function<T>(schema: ValidatorSchemaParam<T>, obj: any): ValidatedObject<T> {
			return Validate.validateObject(obj, schema, { unknown: true });
		};
		v.payload = generateValidator(this.request.payload);
		v.query = generateValidator(this.request.query);
		v.params = generateValidator(this.request.params);
		return v;
	}
}

/// Generates a validator
function generateValidator(obj: any) {
	return function<T>(schema: ValidatorSchemaParam<T>): ValidatedObject<T> {
		return Validate.validateObject(obj, schema, { unknown: true });
	};
}
