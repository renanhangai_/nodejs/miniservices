import { Logger } from "winston";

/// Interface base de um serviço
export interface Service {
	// App
	readonly app: any;
	// Classe Pai
	readonly parent: any;
	// Logger
	readonly logger: Logger;
	// Priority for the service functions
	priority?: number;
	// Service dependency for ordering
	dependencies?: string[];

	/// Faz o setup
	setup?(): any;
	run?(): any;
	close?(): any;
}

/// Generic most basic implementation of the service
export class ServiceBaseGeneric<AppType, ParentType = AppType> implements Service {
	readonly app: AppType;
	readonly parent: ParentType;
	readonly logger: Logger;
}

/// Constructor service type
export type ServiceConstructorType = {
	new (...app: any): Service;
};

/// Constructor service type
export type ServiceConstructorMapType = {
	[key: string]: ServiceConstructorType;
};

/// Service map type for a given service constructor list
export type ServiceMapType<T extends ServiceConstructorMapType> = { [key in keyof T]: InstanceType<T[key]> };
