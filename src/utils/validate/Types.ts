import { StringSchema, NumberSchema, BooleanSchema, DateSchema, ArraySchema } from 'joi';
import { DecimalSchema } from './Decimal';
import { BigNumber } from 'bignumber.js';

/// Schema Base
export type ValidatorSchemaBase = {
	[key: string]: any;
};

/// Validated object value
export type ValidatedObjectValue<T> = T extends StringSchema
	? string
	: T extends NumberSchema
	? number
	: T extends BooleanSchema
	? boolean
	: T extends DateSchema
	? Date
	: T extends DecimalSchema
	? BigNumber
	: T extends ArraySchema
	? any[]
	: any;

/// Um objeto validado a partir de um validador genérico
export type ValidatedObject<T extends ValidatorSchemaBase> = {
	readonly [K in keyof T]: ValidatedObjectValue<T[K]> | null
};
