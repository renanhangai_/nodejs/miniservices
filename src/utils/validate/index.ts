import JoiBase from 'joi';
import { DateExtension } from './Date';
import { ValidatorSchemaBase, ValidatedObject } from './Types';
import { DecimalExtension, DecimalSchema } from './Decimal';

/// Export the validator schema
export { ValidatorSchemaBase, ValidatedObject } from './Types';

// Joi type
type JoiBaseType = typeof JoiBase;
export interface JoiType extends JoiBaseType {
	decimal(): DecimalSchema;
}

/// Joi the validator
export const Joi: JoiType = JoiBase.extend([DecimalExtension, DateExtension]);

/// Context for validator
export type ValidatorContext = {
	Joi: JoiType;
};
export type ValidateObjectOptions = {
	unknown?: boolean;
};
/// Validator schema param
export type ValidatorSchemaParam<T extends ValidatorSchemaBase> = (context: ValidatorContext) => T | T;

/// Validate an object
export function validateObject<T extends ValidatorSchemaBase>(
	data: any,
	schema: ValidatorSchemaParam<T>,
	options?: ValidateObjectOptions
): ValidatedObject<T> {
	options = options || {};
	let normalizedSchema = normalizeSchema(schema);
	if (!normalizedSchema.isJoi) {
		normalizedSchema = Joi.object().keys(normalizedSchema);
	}
	const allowUnknown = options.unknown !== false;
	const { error, value } = Joi.validate(data, normalizedSchema, {
		allowUnknown: allowUnknown,
		stripUnknown: allowUnknown
	});
	if (error) throw error;
	return value;
}

/// Validation object
export const Validate = {
	Joi,
	validateObject
};

/// Normalize the schema
function normalizeSchema<T>(schema: ValidatorSchemaParam<T>): any {
	if (typeof schema === 'function') {
		const context: ValidatorContext = {
			Joi
		};
		return schema(context);
	}
	return schema;
}
