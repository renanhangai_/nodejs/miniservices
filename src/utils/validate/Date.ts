import Joi from "joi";
import fecha from "fecha";

export const DateExtension = (joi: typeof Joi) => ({
	name: "date",
	base: joi.date(),
	language: {
		format: "must be a string with one of the following formats {{format}}"
	},
	/// Converte o valor
	coerce(value, state, options) {
		if (!value || value instanceof Date || typeof value === "number") {
			return value;
		}
		if (this._flags.momentFormat && options.convert) {
			let date = tryParseDateFormats(value, this._flags.momentFormat);
			if (!date) {
				date = new Date(value);
				if (isNaN(date.getTime()))
					return this.createError("date.format", { value, format: this._flags.momentFormat }, state, options);
			}
			return date;
		}
		return value;
	},
	rules: [
		/// Novo método date().format(...)
		{
			name: "format",
			description(params) {
				return `Date should respect format ${params.format}`;
			},
			params: {
				format: joi
					.array()
					.items(joi.string())
					.single()
					.required()
			},
			setup(params) {
				this._flags.momentFormat = params.format;
			},
			validate(params, value, state, options) {
				return value;
			}
		},
		/// Data deverá ser um utc
		{
			name: "utc",
			description(params) {
				return "Date should be interpreted in UTC";
			},
			setup(params) {
				this._flags.utc = true;
			},
			validate(params, value, state, options) {
				return value;
			}
		}
	]
});

function tryParseDateFormats(value, formats) {
	if (!Array.isArray(formats)) {
		formats = [formats];
	}
	for (let i = 0, len = formats.length; i < len; ++i) {
		try {
			let date = fecha.parse(value, formats[i]);
			return date;
		} catch (e) {
			continue;
		}
	}
	return null;
}
