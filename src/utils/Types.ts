export type ConstructorType<T> = {
	new (...args: any[]): T;
};

export type ConstructorInstanceType<T> = T extends ConstructorType<infer U> ? U : any;

/**
 *
 */
export type TypeOrFunctor<T, ThisType = any> = T | ((this: ThisType, ...args: any[]) => T);

export type UnwrapPromise<T> = T extends Promise<infer U> ? U : T;
