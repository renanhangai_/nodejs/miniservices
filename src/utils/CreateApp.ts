import { ServiceBaseGeneric, ServiceConstructorMapType } from '../Service';
import { AppBase } from '../AppBase';
import { ConstructorType, UnwrapPromise } from './Types';

/**
 * Roda o programa a partir da linha de comando
 * @param argv Argumentos da linha de comando
 * @return O programa
 *
 * Instala os sinais e roda o programa
 */
export async function createAppSimple<T extends ServiceConstructorMapType>(
	serviceConstructorMapFn: <U extends ServiceBaseGeneric<AppBase<T>>>(serviceBase: ConstructorType<U>) => T
): Promise<AppBase<T>> {
	class ServiceBase extends ServiceBaseGeneric<App> {}
	class App extends AppBase<T> {
		protected serviceConstructorMap = serviceConstructorMapFn(ServiceBase);
	}
	const app = new App();
	return app.runFromArgv([]);
}

export type CreateAppType<T extends (...args: any[]) => any> = UnwrapPromise<ReturnType<T>>;
