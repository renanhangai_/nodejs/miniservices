import test, { ExecutionContext } from 'ava';
import { validateObject } from '../../../dist/utils/validate';
import fecha from 'fecha';

test('Validator Date Test', t => {
	testDateFormat(t, '31/12/1999', 'DD/MM/YYYY');
	testDateFormat(t, '10/10/2010', 'DD/MM/YYYY');
	testDateFormat(t, '12/31/1999', 'MM/DD/YYYY');
	testDateFormat(t, '10/10/2010', 'MM/DD/YYYY');
	testDateFormat(t, '1999-12-31', 'YYYY-MM-DD');
});

function testDateFormat(t: ExecutionContext, date: string, format: string) {
	const obj = {
		date: date
	};
	const validatedObj = validateObject(obj, ({ Joi }) => ({
		date: Joi.date()
			.format(format)
			.required()
	}));
	t.assert(validatedObj.date instanceof Date, 'Validation result must be a Date');
	t.is(fecha.format(validatedObj.date, format), date, 'Validation result must be equal to input');
}
