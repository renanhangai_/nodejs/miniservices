import test, { ExecutionContext } from 'ava';
import { validateObject } from '../../../dist/utils/validate';
import fecha from 'fecha';
import { DecimalFormatOptions } from '../../../dist/utils/validate/Decimal';
import BigNumber from 'bignumber.js';

test('Validator Decimal Test', t => {
	testDecimal(t, '0,01', '0.01', { decimalSeparator: ',' });
	testDecimal(t, '0|01', '0.01', { decimalSeparator: '|' });
	testDecimal(t, '0.01', '0.01', { decimalSeparator: '.' });
	testDecimal(t, '0,023400', '0.0234', { decimalSeparator: ',' });
});

function testDecimal(t: ExecutionContext, decimal: any, expected: string, format: DecimalFormatOptions) {
	const obj = {
		decimal: decimal
	};
	const validatedObj = validateObject(obj, ({ Joi }) => ({
		decimal: Joi.decimal().format(format)
	}));
	t.assert(validatedObj.decimal instanceof BigNumber, 'Validation result must be a BigNumber');
	t.is(validatedObj.decimal.toString(), expected, 'Validation result must match expected');
}
