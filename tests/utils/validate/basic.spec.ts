import test from 'ava';
import { validateObject, Joi } from '../../../dist/utils/validate';

test('validateObject basic test', t => {
	const obj = {
		number: 10,
		unknownField: null
	};
	const expected = {
		number: 10
	};
	const validatedObj = validateObject(obj, ({ Joi }) => ({
		number: Joi.number().required()
	}));
	t.deepEqual(validatedObj, expected);
});
