import { TestInterface } from "ava";
import { Services, createAppSimple, CreateAppType } from "../../../dist";
import tmp from "tmp-promise";
import fs from "fs-extra";

interface TestContextHapi {
	app: CreateAppType<typeof createApp>;
	socket: string;
}

// Create a basic App to test services
export function setupTestSuiteHapi(test: TestInterface): TestContextHapi {
	const context: TestContextHapi = {
		app: null,
		socket: null
	};
	test.before(async t => {
		context.socket = await tmp.tmpName();
		context.app = await createApp(context.socket);
	});
	test.after(t => context.app.close());
	test.after(t => fs.remove(context.socket));
	return context;
}

async function createApp(socket: string) {
	return createAppSimple(ServiceBase => ({
		hapi: Services.hapi().createService(ServiceBase, {
			server: {
				port: socket
			}
		})
	}));
}
