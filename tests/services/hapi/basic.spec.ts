import test from "ava";
import { setupTestSuiteHapi } from "./suite";

// Create a basic App to test services
const context = setupTestSuiteHapi(test);

/**
 * Basic tests
 */
test("Services.hapi basic tests", async t => {
	t.truthy(context.app.services.hapi, "Hapi service must exist");
});
