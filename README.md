# miniserver

## Installation

```bash
yarn add @renanhangai/miniservices
```

## Getting started

First, define your base application class `App.ts`

```ts
import { AppBase } from "@renanhangai/miniservices";
const ServiceConstructorMap = {};
export class App extends AppBase<typeof ServiceConstructorMap> {
	protected serviceConstructorMap = ServiceConstructorMap;
}
```

Then, define your base service class `Service.ts`

```ts
import { ServiceBaseGeneric } from "@renanhangai/miniservices";

export class ServiceBase extends ServiceBaseGeneric<import("./App").App>;
```

Define your service `Random.ts`

```ts
import { ServiceBase } from "./Service";

export class RandomService extends ServiceBase {
	getRandom(): number {
		return 2;
	}
}
```

Define your service in your app file `App.ts`

```ts
import { AppBase } from "@renanhangai/miniservices";
import { RandomService } from "./Random";
const ServiceConstructorMap = {
	random: RandomService
};
class App extends AppBase<typeof ServiceConstructorMap> {}
```

Run your app `index.ts`

```ts
import { App } from "./App";

async function main() {
	const app = await App.runFromArgv(process.argv);
	console.log(app.services.random.getRandom());
}

main();
```

## Included services

### hapi

Create API endpoints

```ts
import { ServiceBase } from "...";
import { createService, HapiServer } from "@renanhangai/miniservices/lib/hapi";

export const ApiService = createService(ServiceBase, {
	server: {
		port: 80,
		routes: {
			cors: true
		}
	},
	auth(server: HapiServer) {
		// Called before everything, here you must setup the authorization
		// plugins
	},
	register(server: HapiServer) {
		// Calls registration functions for the hapi server or routes
	},
	// The graphql options automatically setups an apollo server
	graphql: {
		// Path where the middleware will be applied
		path: "/graphql",
		// Apollo options for the server
		apollo: { ... },
	}
});
```

### typeorm

Manage typeorm connections

```ts
import { ServiceBase } from "...";
import { createService } from "@renanhangai/miniservices/lib/typeorm";

export const DatabaseService = createService(ServiceBase, {
	default: {
		type: "mysql",
		host: "localhost",
		database: "test",
		username: "root",
		password: "root",
		entities: [ ... ],
	}
});
```

```ts
// Gets the default connection (alias for service.connections.default)
service.connection;
// Object with all the connections
service.connections;
```
