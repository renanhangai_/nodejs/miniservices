# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.5.3](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.5.2...v3.5.3) (2019-06-15)



### [3.5.2](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.5.1...v3.5.2) (2019-06-05)


### Bug Fixes

* Resolver of data on context ([b4eb8cb](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/b4eb8cb))



### [3.5.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.5.0...v3.5.1) (2019-06-05)


### Bug Fixes

* RouteManager context can now be async ([a2f09b5](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/a2f09b5))



## [3.5.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.4.0...v3.5.0) (2019-06-05)


### Features

* Added data options when creating the route manager ([9d44e63](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/9d44e63))



## [3.4.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.3.1...v3.4.0) (2019-06-04)


### Bug Fixes

* runFromArgv on createAppSimple ([a38453d](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/a38453d))


### Features

* Added option to run from options ([a24e3ad](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/a24e3ad))



### [3.3.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.3.0...v3.3.1) (2019-06-03)


### Bug Fixes

* Export of route types ([38c74b8](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/38c74b8))



## [3.3.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.2.4...v3.3.0) (2019-06-03)


### Features

* Added utility type to route context params ([decc504](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/decc504))



### [3.2.4](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.2.3...v3.2.4) (2019-06-03)


### Bug Fixes

* Missing thousands separator from Joi.decimal().format() ([956a85c](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/956a85c))



### [3.2.3](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.2.2...v3.2.3) (2019-06-03)


### Bug Fixes

* Decimal validation when null value is passed ([493ef28](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/493ef28))



### [3.2.2](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.2.1...v3.2.2) (2019-05-27)



### [3.2.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.2.0...v3.2.1) (2019-05-27)


### Bug Fixes

* Field on query resolver was made optional ([4b79cc3](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/4b79cc3))
* ValueType on where resolvers ([71f9849](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/71f9849))



## [3.2.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.1.0...v3.2.0) (2019-05-27)


### Features

* Updated resolver to use an object as options ([d61f206](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/d61f206))



## [3.1.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v3.0.0...v3.1.0) (2019-05-27)


### Features

* Updated schema validation ([d7f90e6](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/d7f90e6))



## [3.0.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.1.2...v3.0.0) (2019-05-27)


### Features

* Added Joi parameter to options on resolver ([9be4999](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/9be4999))


### BREAKING CHANGES

* Older resolvers cannot be used alogside this method



### [2.1.2](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.1.1...v2.1.2) (2019-05-27)



## [2.1.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.1.0...v2.1.1) (2019-05-21)



# [2.1.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.0.3...v2.1.0) (2019-05-21)


### Features

* Updated route request type definition ([d5e9e83](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/d5e9e83))



## [2.0.3](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.0.2...v2.0.3) (2019-05-21)



## [2.0.2](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.0.1...v2.0.2) (2019-05-21)



## [2.0.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.0.0...v2.0.1) (2019-05-21)


### Bug Fixes

* Request optons ([c6ab033](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/c6ab033))



# [2.0.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.0.0-alpha.2...v2.0.0) (2019-05-21)


### Features

* Added plugin for route manager ([7279112](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/7279112))



# [2.0.0-alpha.2](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.0.0-alpha.1...v2.0.0-alpha.2) (2019-05-18)



# [2.0.0-alpha.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v2.0.0-alpha.0...v2.0.0-alpha.1) (2019-05-18)



# [2.0.0-alpha.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.4.0-alpha.0...v2.0.0-alpha.0) (2019-05-18)



# [1.4.0-alpha.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.3.0...v1.4.0-alpha.0) (2019-05-18)


### Features

* Moved hapi service to the service table ([d58b181](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/d58b181))



# [1.3.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.1.2...v1.3.0) (2019-05-16)


### Bug Fixes

* Close method of server ([7f3db9c](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/7f3db9c))


### Features

* Added better support for entityManager getter on query definition ([36b0618](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/36b0618))



# [1.2.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.1.2...v1.2.0) (2019-05-07)


### Features

* Added better support for entityManager getter on query definition ([36b0618](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/36b0618))



## [1.1.2](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.1.1...v1.1.2) (2019-05-06)



## [1.1.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.1.0...v1.1.1) (2019-05-06)



# [1.1.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.12...v1.1.0) (2019-05-06)


### Features

* Added new QueryHelper for typeorm to build resolvers ([d748ea6](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/d748ea6))



## [1.0.12](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.11...v1.0.12) (2019-04-29)


### Bug Fixes

* When apollo option is a function on graphql, await for it ([c5fe073](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/c5fe073))



<a name="1.0.11"></a>
## [1.0.11](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.10...v1.0.11) (2019-04-10)


### Bug Fixes

* app, parent and logger setup on services ([c56357d](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/c56357d))
* Log of async errors on typeorm ([1a772c8](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/1a772c8))



<a name="1.0.10"></a>
## [1.0.10](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.9...v1.0.10) (2019-04-10)


### Bug Fixes

* Logger for typeorm failure attempts ([b404290](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/b404290))



<a name="1.0.9"></a>
## [1.0.9](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.8...v1.0.9) (2019-04-10)



<a name="1.0.8"></a>
## [1.0.8](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.7...v1.0.8) (2019-04-10)



<a name="1.0.7"></a>
## [1.0.7](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.6...v1.0.7) (2019-04-10)


### Bug Fixes

* Fixed auth registration on hapi service ([cd19d71](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/cd19d71))



<a name="1.0.6"></a>
## [1.0.6](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.5...v1.0.6) (2019-04-10)


### Bug Fixes

* Implementation of Service on services constructed by the library ([bcbc5d4](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/bcbc5d4))



<a name="1.0.5"></a>
## [1.0.5](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.4...v1.0.5) (2019-04-10)


### Bug Fixes

* Changed tsconfig of library to target es6 ([d45ca81](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/d45ca81))



<a name="1.0.4"></a>
## [1.0.4](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.3...v1.0.4) (2019-04-10)


### Bug Fixes

* Invalid creation of connection when options is null ([c1d026a](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/c1d026a))



<a name="1.0.3"></a>
## [1.0.3](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.2...v1.0.3) (2019-04-10)


### Bug Fixes

* Removed entitiesMap from the createService on typeorm ([601d89b](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/601d89b))



<a name="1.0.2"></a>
## [1.0.2](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.1...v1.0.2) (2019-04-10)


### Bug Fixes

* createService method on typeorm service ([149d205](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/149d205))



<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v1.0.0...v1.0.1) (2019-04-10)



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.1.1...v1.0.0) (2019-04-10)


### Bug Fixes

* Dependencies now are resolved after instantiation and cached ([372f735](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/372f735))


### Features

* Added new utility services ([30284cf](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/30284cf))



<a name="0.1.1"></a>
## [0.1.1](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.1.0...v0.1.1) (2019-04-07)


### Bug Fixes

* Dependency priority order ([d80ff3d](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/d80ff3d))



<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.0.10...v0.1.0) (2019-04-07)


### Features

* New dependency member of services for ordering. ([e9e0c6b](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/e9e0c6b))



<a name="0.0.10"></a>
## [0.0.10](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.0.9...v0.0.10) (2019-04-05)


### Bug Fixes

* Constructor of logger ([cce20e8](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/cce20e8))



<a name="0.0.9"></a>
## [0.0.9](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.0.8...v0.0.9) (2019-04-05)



<a name="0.0.8"></a>
## [0.0.8](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.0.7...v0.0.8) (2019-04-05)



<a name="0.0.7"></a>
## [0.0.7](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.0.6...v0.0.7) (2019-04-05)



<a name="0.0.6"></a>
## [0.0.6](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.0.5...v0.0.6) (2019-04-05)


### Bug Fixes

* Declaration of runFromArgv ([76db45f](https://gitlab.com/renanhangai_/nodejs/miniservices/commit/76db45f))



<a name="0.0.5"></a>
## [0.0.5](https://gitlab.com/renanhangai_/nodejs/miniservices/compare/v0.0.4...v0.0.5) (2019-04-03)



<a name="0.0.4"></a>
## [0.0.4](https://gitlab.com/renanhangai_/nodejs/miniserver/compare/v0.0.3...v0.0.4) (2019-04-03)



<a name="0.0.3"></a>
## [0.0.3](https://gitlab.com/renanhangai_/nodejs/miniserver/compare/v0.0.2...v0.0.3) (2019-04-03)


### Bug Fixes

* Service runner was not waiting ([2b4245e](https://gitlab.com/renanhangai_/nodejs/miniserver/commit/2b4245e))



<a name="0.0.2"></a>
## [0.0.2](https://gitlab.com/renanhangai_/nodejs/miniserver/compare/v0.0.1...v0.0.2) (2019-04-02)



<a name="0.0.1"></a>
## 0.0.1 (2019-04-02)
